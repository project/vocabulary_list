Drupal vocabulary_list.module README.txt
==============================================================================

This module plugs a small hole in taxonomy.module, providing node list
pages and RSS services for vocabularies. These pages are accessible as

  taxonomy/page/vocab/{vid} and
  taxonomy/feed/vocab/{vid}

Only a single vocabulary can be acccessed (no logical magic as with terms
in taxonomy module). RSS autodiscovery is supported.

If you feel this functionality should go into Drupal core, go on and provide
a patch for Drupal core. I have added this module to complement taxonomy
module in Drupal 4.4 (for which it is not possible to add new features).

Installation
------------------------------------------------------------------------------
 
 Required:
  - Copy vocablary_list.module to modules/
  - Enable the module as usual from Drupal's admin pages.

Credits / Contact
------------------------------------------------------------------------------

The author of this module is Gabor Hojtsy (goba[at]php.net), who is also
the active maintainer.
